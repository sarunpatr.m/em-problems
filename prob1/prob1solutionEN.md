The problem is a point charge above a conducting plane. We need to apply the image theory by replacing conducting plane with the image of the point change as shown in figure below.

> We need image of the equivalent problem. Please take the file for the [orginal problem](/asset/goodnotes/Point-charge-image-theory-DCW.goodnotes) and modify it to the equivalent problem.

The equation for electric field at a point $`(x, y, z)`$ produced by a point charge located at $`(x_o, y_o, z_o)`$ is  below
```math
\bold {E} = \frac{Q}{4 \pi \varepsilon} \frac {\bold {r-r'}}{|\bold {r-r'}|^3}
``` 
